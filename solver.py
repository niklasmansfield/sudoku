import curses
import ui
from assets import get_problem, get_empty_candidate_matrix
from sudoku_tests import ex

@ex(1)
def number_in_row(grid, number, row):
	for i in range(9):
		if number == grid[row][i]:
			return True
	return False


@ex(2)
def number_in_column(grid, number, col):
	for i in range(9):
		if number == grid[i][col]:
			return True
	return False


@ex(3)
def sq_coord_to_block_coord(row, col):
	return row // 3, col // 3


@ex(4)
def number_in_block(grid, number, b_row, b_col):
	rows = range(b_row*3, b_row*3 + 3)
	cols = range(b_col*3, b_col*3 + 3)
	for y in rows:
		for x in cols:
			if number == grid[y][x]:
				return True
	return False


@ex(5)
def possible_to_place(grid, number, row, col):
	b_row, b_col = sq_coord_to_block_coord(row, col)
	if grid[row][col] == 0:
		if not (number_in_row(grid, number, row) or number_in_column(grid, number, col) or number_in_block(grid, number, b_row, b_col)):
			return True
	return False


@ex(6)
def get_pencil_list(grid, row, col):
	possible_values = []
	for i in range(1,10):
		if possible_to_place(grid, i, row, col):
			possible_values.append(i)
	return possible_values


@ex(7)
def solve_for_single_candidate(grid, row, col):
	if len(get_pencil_list(grid, row, col)) == 1:
		return get_pencil_list(grid, row, col)[0]
	else:
		return None


@ex(8)
def finito(grid):
	for row in range(9):
		for col in range(9):
			if grid[row][col] == 0:
				return False
	return True


@ex(9)
def single_position_row(grid, number, row, col):
	found_occurrence = False
	for x in range(9):
		if possible_to_place(grid, number, row, x):
			if found_occurrence:
				return False
			else:
				found_occurrence = True
	return found_occurrence
			

@ex(10)
def single_position_column(grid, number, row, col):
	found_occurrence = False
	for y in range(9):
		if possible_to_place(grid, number, y, col):
			if found_occurrence:
				return False
			else:
				found_occurrence = True
	return found_occurrence
			

@ex(11)
def single_position_block(grid, number, row, col):
	found_occurrence = False
	
	b_row, b_col = sq_coord_to_block_coord(row, col)
	rows = range(b_row*3, b_row*3 + 3)
	cols = range(b_col*3, b_col*3 + 3)
	
	for y in rows:
		for x in cols:
			if possible_to_place(grid, number, y, x):
				if found_occurrence:
					return False
				else:
					found_occurrence = True
	return found_occurrence
			

@ex(12)
def solve_for_single_position(grid, row, col):
	pencil_list = get_pencil_list(grid, row, col)
	for number in pencil_list:
		if single_position_row(grid, number, row, col) or single_position_column(grid, number, row, col) or single_position_block(grid, number, row, col):
			return number
	return None



def main(stdscreen):
    
    terminal_ui = ui.SudokuUI(stdscreen, True)
    
    grid = get_problem(3)
    
    terminal_ui.print_problem(grid)
    terminal_ui.wait_for_input()
    
    candidates = get_empty_candidate_matrix()
    terminal_ui.print_candidates(grid, candidates)
    terminal_ui.wait_for_input()
    
    # write main loop here

    while not finito(grid):
        for row in range(9):
            for col in range(9):
                if grid[row][col] == 0:
                    number = solve_for_single_candidate(grid, row, col)
                    if number == None:
                        number = solve_for_single_position(grid, row, col)
                    if number != None:
                        terminal_ui.update_problem(row, col, number)
                        grid[row][col] = number
						
                                                		
            terminal_ui.print_problem(grid)
        terminal_ui.wait_for_input()


if __name__ == "__main__":    
	curses.wrapper(main)
